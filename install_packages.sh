#!/bin/bash

# enable epel
wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
sudo rpm -Uvh remi-release-6*.rpm epel-release-6*.rpm

yum update

yum -y groupinstall "Development Tools"

# All these ceremonies are needed because cmake 2.6 is being installed by default.
# For details see http://www.cmake.org/cmake/resources/software.html
# also it is neccessary to add /usr/local/bin to PATH:
export PATH=$PATH:/usr/local/bin
# TODO: add permanentely as there:
# http://www.how-to-linux.com/centos-52/how-to-add-usrlocalbin-to-your-path-on-centos/
cd /root
wget 'http://www.cmake.org/files/v2.8/cmake-2.8.12.1.tar.gz'
tar xvfz cmake-2.8.12.1.tar.gz
cd cmake-2.8.12.1
./bootstrap
make prefix=/usr/local all
make prefix=/usr/local install

# install git 1.8 to avoid problems with push
cd /root
yum -y install zlib-devel openssl-devel cpio expat-devel gettext-devel curl-devel perl-ExtUtils-CBuilder perl-ExtUtils-MakeMaker
wget -O v1.8.1.2.tar.gz https://github.com/git/git/archive/v1.8.1.2.tar.gz
tar -xzvf ./v1.8.1.2.tar.gz
cd git-1.8.1.2/
make prefix=/usr/local all
make prefix=/usr/local install

# Install openmpi
yum -y install openmpi-devel
# magic, see https://www.centos.org/forums/viewtopic.php?t=2235
module add openmpi-x86_64

# TODO: apply install_boost.sh to install boost for c++11,
# see article http://joelinoff.com/blog/?p=1003. TONOTE: take 5 hours !!!???
# else for usual c++ make the following steps 
# (see http://www.boost.org/doc/libs/1_54_0/doc/html/bbv2/installation.html):

# sudo -s
# cd /root
# wget 'http://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.tar.bz2'
# find . -name 'boost*.tar.bz2' -exec tar -xvjf {} \;
# cd `find . -maxdepth 1 -type d -name 'boost*'`
# ./bootstrap.sh
# ./b2

# install python 2.6 (to upgrade, see https://github.com/0xdata/h2o/wiki/Installing-python-2.7-on-centos-6.3.-Follow-this-sequence-exactly-for-centos-machine-only)
yum install python
yum install python-devel

# install python 2.7.2 into home directory, to embed into c++
# see http://stackoverflow.com/questions/8282231/ubuntu-i-have-python-but-gcc-cant-find-python-h
# wget http://python.org/ftp/python/2.7.2/Python-2.7.2.tar.bz2
# tar xjf Python-2.7.2.tar.bz2
# cd Python-2.7.2
# ./configure --prefix=/home/vagrant/python --enable-unicode=ucs4
# make
# make install
# now it is possible to use -I /home/username/python/include to include Python.h and
# -L /home/username/python/lib and -lpython2.7 when linking, e.g
# g++ main.c -I/usr/include/python2.6/ -lpython2.6 -o main